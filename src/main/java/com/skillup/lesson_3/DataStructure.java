package com.skillup.lesson_3;

import java.util.*;

public class DataStructure {

    public static void main(String[] args) {


        int[] array = {1, 2, 3};
        int[] array2 = new int[3];//0, 1, 2 => 3 elements


        array2[0] = 1;
        array2[2] = 7;
        array2[1] = 10;

//        for (int asint : array2) {
//            System.out.println(asint);
//        }

//        System.out.println(array2[2] + " , " + array2[1]);

//        for ( int i=0; i< array2.length;i++)
//        {
//            System.out.println(array2[i]);
//        }


//        System.out.println(array[1]);
//
//        for (int i = 0; i < array.length; i++) {
//            System.out.println(array[i]);
//        }
//
//        //Advanced way
//        for (int asint : array) {
//            System.out.println(asint);
//        }
//
//
////        System.arraycopy(array, 0, array2, 0, 3);
//
//        System.out.println("-------------------------");


        List<String> stringList = new ArrayList<String>();
        List<String> linkedList = new LinkedList<String>();

        Set<String> set = new HashSet<String>();
        Set<String> set2 = new LinkedHashSet<String>();


        stringList.add("1");
        stringList.add("2");
        stringList.add("3");
        stringList.add("4");
//
//        stringList.add(0, "");
//        stringList.remove();
//        stringList.size();
//        stringList.get();
//
//        for (int i = 0; i < stringList.size(); i++) {
//            System.out.println(stringList.get(i));
//        }
////
//        doSmth(stringList);
//        doSmth(linkedList);
////
////    }
////
//    public static void doSmth(List<String> list) {
////
//    }
    }
}


