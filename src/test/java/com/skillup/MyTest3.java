package com.skillup;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MyTest3 extends TestRunner {
    private static final String URL = "https://client.cryptobriefing.maxiemind.com";
    private static final String EXPECTED_TITLE = "Crypto Briefing";
    private static final String UNEXPECTED_TITLE = "Something wrong";
    private static final String EXPECTED_URL = "https://client.cryptobriefing.maxiemind.com/dashboard/listed/";
    private static final String EXPECTED_URL_TEXT = "cryptobriefing";
    private static final String UNEXPECTED_URL = "/something Unexpected.com";
    private static final String UNEXPECTED_URL_TEXT = "Strange Text";
    private static final String STRING_PATTERN = "text %s %s %s another text";


    @Test
    public void myTitleTest() {
        driver.get(URL);
        String actualTitle = driver.getTitle();

        //True
        Assert.assertEquals(actualTitle, EXPECTED_TITLE, "SITE TITLE IS WRONG!");

    }

    @Test
    public void negativeTitleTest() {
        driver.get(URL);
        String actualTitle = driver.getTitle();

        //False
        Assert.assertFalse(actualTitle.equals(UNEXPECTED_TITLE), "What's this?");
    }

    @Test
    public void url_text_Test() {
        driver.get(URL);
        String actualUrl = driver.getCurrentUrl();

        //True
        Assert.assertTrue(actualUrl.contains(EXPECTED_URL_TEXT),
                String.format("This url doesn't contain expected string, " +
                        "actual url: %s, expected part: %s", actualUrl, EXPECTED_URL_TEXT));

    }


    @Test
    public void url_text_negative_Test() {
        driver.get(URL);
        String actualUrl = driver.getCurrentUrl();

        //False
        Assert.assertFalse(actualUrl.contains(UNEXPECTED_URL_TEXT), "What's this?");
    }

//    @Test(expectedExceptions = ArithmeticException.class)
//    public void test(){
//        Calculator calculator = new Calculator();
//
//        calculator.divide(1, 0);
//    }

//    public static void main(String[] args) {
//        System.out.println(String.format(STRING_PATTERN, "1234", "2", "3"));
//    }
}

