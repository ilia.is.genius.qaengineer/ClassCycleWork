package com.skillup;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class TestRunner {

    protected WebDriver driver;


    @BeforeSuite
    public void BeforeSuite() {
        System.out.println("Before Suite");
        WebDriverManager.chromedriver().setup();
    }


    @BeforeClass
    public void BeforeClass() {
        System.out.println("Before Class");
    }

    @BeforeMethod
    public void BeforeMethod() {
        System.out.println("Before Method");
        driver = new ChromeDriver();
    }

    @AfterSuite
    public void AfterSuite() {
        System.out.println("After Suite");
    }

    @AfterClass
    public void AfterClass() {
        System.out.println("After Class");
    }

    @AfterMethod
    public void AfterMethod() {
        System.out.println("After Method");
        driver.quit();
    }
}
